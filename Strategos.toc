## Interface: 90005
## Title: Strategos
## Dependencies: EmeraldFramework
## Notes: Enhances your Battleground experience.
## SavedVariables: StrategosSettings
## SavedVariablesPerCharacter: StrategosProfile
## Version: 0.1
## Author: WobLight
Localization.lua
Strategos.lua
Battleground.lua
UIElements.lua
WarsongGulch.lua
AlteracValley.lua
ArathiBasin.lua
Localization_deDE.lua

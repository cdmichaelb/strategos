local addonName, addonTable = ...

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

EFrame.newClass("AlteracValley", Battleground)

AlteracValley.id = 30
idLookups[AlteracValley.id] = AlteracValley

ALTERAC_VALLEY_ZONENAME = tr("Alterac Valley", "Zone name as reported by `/run message(GetSubZoneText())` at alterac entrance (in battleground)")

ALTERAC_VALLEY_CAPTURE_TIME = WOW_PROJECT_ID == WOW_PROJECT_CLASSIC and 300 or 240

local AlteracBases = {
    [tr("Irondeep Mine")] = {
        x = .5,
        y = 0.0875,
        type = Node.Mine
    },
    [tr("Coldtooth Mine")] = {
        x = 5/6,
        y = 0.0875,
        type = Node.Mine
    },
    [tr("West Frostwolf Tower")] = {
        x = .5,
        y = 0.9,
        type = Node.Tower
    },
    [tr("East Frostwolf Tower")] = {
        x = 5/6,
        y = 0.9,
        type = Node.Tower
    },
    [tr("Iceblood Tower")]       = {
        x = 5/6,
        y = 0.7375,
        type = Node.Tower
    },
    [tr("Tower Point")]          = {
        x = .5,
        y = 0.7375,
        type = Node.Tower
    },
    [tr("Iceblood Graveyard")]   = {
        x = 1/6,
        y = 0.60,
        type = Node.Graveyard
    },
    [tr("Frostwolf Graveyard")]  = {
        x = 1/6,
        y = 0.75,
        type = Node.Graveyard
    },
    [tr("Frostwolf Relief Hut")] = {
        x = 1/6,
        y = 0.9,
        type = Node.Graveyard
    },
    [tr("Dun Baldar North Bunker")] = {
        x = .5,
        y = 0.25,
        type = Node.Tower
    },
    [tr("Dun Baldar South Bunker")] = {
        x = 5/6,
        y = 0.25,
        type = Node.Tower
    },
    [tr("Icewing Bunker")]       = {
        x = .5,
        y = 0.4125,
        type = Node.Tower
    },
    [tr("Stonehearth Bunker")]   = {
        x = 5/6,
        y = 0.4125,
        type = Node.Tower
    },
    [tr("Stormpike Aid Station")] = {
        x = 1/6,
        y = 0.15,
        type = Node.Graveyard
    },
    [tr("Stormpike Graveyard")]  = {
        x = 1/6,
        y = 0.3,
        type = Node.Graveyard
    },
    [tr("Stonehearth Graveyard")] = {
        x = 1/6,
        y = 0.45,
        type = Node.Graveyard
    },
    [tr("Snowfall Graveyard")]   = {
        x = 0.6625,
        y = 0.575,
        type = Node.Graveyard
    }
}

local EFrame = EFrame.Blizzlike
function AlteracValley:new()
    Battleground.new(self)
    self.startTimer:clearAnchors()
    self.startTimer.marginTop = 25
    self.startTimer.anchorTop = EFrame.root.top
    self.startTimer.width = 40
    self.startTimer.height = 40
    self.window = EFrame.Window()
    self.window.title = tr("Alterac Valley", "Window title")
    self.window.minimumHeight = 1
    self.window.centralItem = EFrame.Item(self.window)
    self.window.centralItem.implicitHeight = 195
    for k, v in pairs(AlteracBases) do
        local n = Node(k, v.type, self)
        local i = EFrame.Image(self.window.centralItem)
        i.anchorCenter = self.window.topLeft
        i.hoffset = EFrame.bind(function() return v.x * self.window.width end)
        i.voffset = EFrame.bind(function() return -v.y * self.window.height end)
        i.implicitWidth = 12
        i.implicitHeight = 12
        i.source = "Interface\\Minimap\\POIIcons.blp"
        function i:updateTexture(d)
            x, y = d%8, math.floor(d/8)
            i:setCoords(x/8,(x+1)/8,y/8,(y+1)/8)
        end
        n:connect("textureIndexChanged", i, "updateTexture")
        i:updateTexture(n.textureIndex)
        i.z=4
        i.opacity = EFrame.bind(function() return bit.band(n.status, Node.Contested) ~= 0 and 1 or .75 end)
        local t = SpinningCountdown(self.window.centralItem)
        t.implicitWidth = 24
        t.implicitHeight = 24
        t.anchorCenter = i.center
        t.message = n.name
        t.opacity = 0
        
--         if n.type == Node.Graveyard then
--             n:connect("ressoffChanged", function() t:restart(n:spiritHealerTime(), tresstime) end)
--         end
        n:connect("assaulted", function(time) t:restart(time, ALTERAC_VALLEY_CAPTURE_TIME) end)
        n:connect("statusChanged", function (s)
        --    if n.type == Node.Graveyard and s == playerFaction then
       --         t:restart(n:spiritHealerTime(), tresstime)
       --     else
                t:stop()
       --     end
        end)
      --  t.looping = EFrame.bind(function() return n.status == playerFaction end)
        self.nodes[k] = n
    end
    self.nodes[ALTERAC_VALLEY_ZONENAME] = Node(ALTERAC_VALLEY_ZONENAME, Node.Graveyard, self)
    self.nodes[ALTERAC_VALLEY_ZONENAME].status = playerFaction
    self.window.centralItem.model = EFrame.ListModel(self)
    if StrategosProfile.alteracWindowHeight then
        self.window.height = EFrame.normalizeBind(StrategosProfile.alteracWindowHeight)
    end
    if StrategosProfile.alteracWindowX then
        self.window.marginLeft = EFrame.normalizeBind(StrategosProfile.alteracWindowX)
    end
    if StrategosProfile.alteracWindowY then
        self.window.marginTop = EFrame.normalizeBind(StrategosProfile.alteracWindowY)
    end
    self.window:connect("marginTopChanged", function(v) StrategosProfile.alteracWindowY = EFrame.normalized(v) end)
    self.window:connect("marginLeftChanged", function(v) StrategosProfile.alteracWindowX = EFrame.normalized(v) end)
    self.window:connect("heightChanged", function(v) if self.window.implicitHeight ~= v then StrategosProfile.alteracWindowHeight = EFrame.normalized(v) end end)
    self.b = SpiritHealerIndicator()
    self.b.visible = EFrame.bind(function() return self.status ~= Battleground.Starting end)
    if WOW_PROJECT_ID == WOW_PROJECT_CLASSIC then
        self.b.marginTop = 25
        self.b.anchorTop = EFrame.root.top
    else
        local anchorFrame = UIWidgetTopCenterContainerFrame
        self.b.anchorTopLeft = {frame = anchorFrame, point = "BOTTOMRIGHT"}
    end
    self:updateWorldStates()
end

function AlteracValley:destroy()
    self.b:destroy()
    self.window:destroy()
    Battleground.destroy(self)
end

local evs = {
    [tr("1 minute.*Alterac Valley")] = function (self)
        self.status = Battleground.Starting
        self:starting(60)
    end,
    [tr("30 seconds.*Alterac Valley")] = function (self)
        self.status = Battleground.Starting
        self:starting(30)
    end,
    [tr("Alterac Valley.* has begun")] = function (self)
        self.status = nil
        self:started()
    end,
    [tr("^(.*) is under attack!%s+If left")] = function (self, message, faction, node)
        node = string.gsub(node, tr("^The ", "remove article in \"^(.*) is under attack!%s+If left\""),"")
        self.nodes[node]:assault(ALTERAC_VALLEY_CAPTURE_TIME, faction)
    end,
    [tr("claims (.*) graveyard")] = function (self, message, faction, node)
        node = string.gsub(node, tr("^the ", "remove article in \"claims (.*) graveyard\""),"")
        self.nodes["Snowfall Graveyard"]:assault(ALTERAC_VALLEY_CAPTURE_TIME, faction)
    end,
    [tr("^(.*) was taken by")] = function (self, message, faction, node)
        node = string.gsub(node, tr("^The ", "remove article in \"^(.*) was taken by\""),"")
        self.nodes[node]:capture(faction)
    end,
    [tr("^(.*) was destroyed by")] = function (self, message, faction, node)
        node = string.gsub(node, tr("^The ", "\"^(.*) was destroyed by\""), "")
        self.nodes[node]:capture(Node.Neutral)
    end
}

function AlteracValley:processChatEvent(message, faction)
    for s,f in pairs(evs) do
        local r = {strfind(message, s)}
        if #r > 0 then
            f(self, message, faction, select(3,unpack(r)))
            return
        end
    end
    Battleground.processChatEvent(self, message, faction)
end


function AlteracValley:updateWorldStates()
    if not self.b.ok then
        self.b:restart(self.nodes[ALTERAC_VALLEY_ZONENAME])
        self.b.ok = true
    end
    local a = C_Map.GetBestMapForUnit("player")
    if a then
        for _,v in pairs(C_AreaPoiInfo.GetAreaPOIForMap(a)) do
            local i = C_AreaPoiInfo.GetAreaPOIInfo(a, v)
            if self.nodes[i.name] then
                self.nodes[i.name].status = POIInfo[i.textureIndex].status
            end
        end
    end
end

local grave = setmetatable({
    [tr("Dun Baldar")] = tr("Stormpike Aid Station"),
    [tr("Frostwolf Keep")] = tr("Frostwolf Relief Hut"),
    [tr("Winterax Hold")] = tr("Snowfall Graveyard"),
    [""] = ALTERAC_VALLEY_ZONENAME
}, { __index = function(_,k) return k end })

local lastSpiritHealerTime = 0
function AlteracValley:update()
    local node = self.nodes[grave[GetSubZoneText()]]
    local time = GetAreaSpiritHealerTime()
    if lastSpiritHealerTime and lastSpiritHealerTime ~= time and time > 1 and time < 30 then
        if not node then return end
        lastSpiritHealerTime = time
        local off = node:spiritHealerTime() - time -1
        if math.abs(off) > .25 then
            node.ressoff = node.ressoff + off
        end
    elseif time ~= 0 then
        lastSpiritHealerTime = time
    else
        lastSpiritHealerTime = nil
    end
    if node and self.b.node ~= node and node.type == Node.Graveyard and node.status == playerFaction and (GetAreaSpiritHealerTime() > 0 or node.name ~= ALTERAC_VALLEY_ZONENAME) then
        self.b:restart(node)
    end
end

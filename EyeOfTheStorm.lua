local addonName, addonTable = ...

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable)

EFrame.newClass("EyeOfTheStorm", Battleground)
EyeOfTheStorm:attach("flagCarrier")

EyeOfTheStorm.id = 566
idLookups[EyeOfTheStorm.id] = EyeOfTheStorm

EYE_OF_THE_STORM_ZONENAME = tr("Eye of the Storm", "Zone name as reported by `/run message(GetSubZoneText())` at arathi entrance (in battleground)")

local carriersIds = {"arena1", "arena2"}

local EyeBases = {
    [tr("Mage Tower")] = {
        x = .25,
        y = .25,
        type = Node.Tower
    },
    [tr("Draenei Ruins")] = {
        x = .75,
        y = .25,
        type = Node.Tower
    },
    [tr("Fel Reaver Ruins")] = {
        x = .25,
        y = .75,
        type = Node.Tower
    },
    [tr("Blood Elf Tower")]       = {
        x = .75,
        y = .75,
        type = Node.Tower
    },
}

EyeOfTheStorm.AllianceAhead = Alliance
EyeOfTheStorm.HordeAhead = Horde
EyeOfTheStorm.Stalling = 0

EyeOfTheStorm:attach("allianceTimeToWin")
EyeOfTheStorm:attach("hordeTimeToWin")
EyeOfTheStorm:attach("winningTeam")
EyeOfTheStorm.__allianceTimeToWin = 0
EyeOfTheStorm.__hordeTimeToWin = 0
EyeOfTheStorm.__winningTeam = EyeOfTheStorm.Stalling


AB_BAR_COLORS = {
    [EyeOfTheStorm.Stalling] = {1,1,1},
    [EyeOfTheStorm.AllianceAhead] = {0,0,1},
    [EyeOfTheStorm.HordeAhead] = {1,0,0}
}

AB_BAR_BG_COLORS = {
    [EyeOfTheStorm.Stalling] = {1,1,1,.25},
    [EyeOfTheStorm.AllianceAhead] = {1,0,0,.25},
    [EyeOfTheStorm.HordeAhead] = {0,0,1,.25}
}

local EFrame = EFrame.Blizzlike
function EyeOfTheStorm:new(debug)
    Battleground.new(self)
    self.flags = {Flag(self), Flag(self)}
    self.carriers = {Carrier(self), Carrier(self)}
    self.flags[Alliance].carrier = self.carriers[Horde]
    self.flags[Horde].carrier = self.carriers[Alliance]
    self.carriers[Horde].faction = Horde
    self.carriers[Alliance].faction = Alliance
    self.l1 = FlagFrame(self, self.flags[Horde], Alliance, 2)
    self.l2 = FlagFrame(self, self.flags[Alliance], Horde, 1)
    self.l1.width = EFrame.bind(function() return math.max(self.l1.implicitWidth, self.l2.implicitWidth) end)
    self.l2.width = EFrame.bind(function() return math.max(self.l1.implicitWidth, self.l2.implicitWidth) end)
    self.l1.marginLeft = EFrame.bind(function() return EFrame.normalizeUI(EFrame.normalize(120)) end)
    self.l2.marginLeft = EFrame.bind(function() return EFrame.normalizeUI(EFrame.normalize(120)) end)
    local anchorFrame = debug or UIWidgetTopCenterContainerFrame
    self.l1.anchorTopLeft = {frame = anchorFrame, point = "TOPRIGHT"}
    self.l1.height = EFrame.bind(function() return anchorFrame:GetHeight() * 0.42 * UIParent:GetScale() / EFrame.root.scale end)
    self.l2.anchorBottomLeft = {frame = anchorFrame, point = "BOTTOMRIGHT"}
    self.l2.height = EFrame.bind(function() return anchorFrame:GetHeight() * 0.42 * UIParent:GetScale() / EFrame.root.scale end)
    
    self.window = EFrame.Window()
    self.window.title = tr("Eye of the Storm", "Window title")
    self.window.minimumHeight = 1
    self.window.centralItem = EFrame.Item(self.window)
    self.window.centralItem.implicitHeight = 195
    for k, v in pairs(EyeBases) do
        local n = Node(k, v.type, self)
        local i = EFrame.Image(self.window.centralItem)
        i.anchorCenter = self.window.topLeft
        i.hoffset = EFrame.bind(function() return v.x * self.window.width end)
        i.voffset = EFrame.bind(function() return -v.y * self.window.height end)
        i.implicitWidth = EFrame.bind(function() return self.window.width * 0.15 end)
        i.implicitHeight = EFrame.bind(function() return self.window.height * 0.15 end)
        i.source = "Interface\\Minimap\\POIIcons.blp"
        function i:updateTexture(d)
            x, y = d%8, math.floor(d/8)
            i:setCoords(x/8,(x+1)/8,y/8,(y+1)/8)
        end
        n:connect("textureIndexChanged", i, "updateTexture")
        i:updateTexture(n.textureIndex)
        i.z=4
        i.opacity = EFrame.bind(function() return bit.band(n.status, Node.Contested) ~= 0 and 1 or .75 end)
        local t = SpinningCountdown(self.window.centralItem)
        t.implicitWidth = EFrame.bind(function() return i.width * 2 end)
        t.implicitHeight = EFrame.bind(function() return i.height * 2 end)
        t.anchorCenter = i.center
        t.message = n.name
        t.opacity = 0
        
        n:connect("assaulted", function(time) t:restart(time, 60) end)
        n:connect("statusChanged", function (s)
            t:stop()
        end)
        self.nodes[k] = n
    end
    self.nodes[EYE_OF_THE_STORM_ZONENAME] = Node(EYE_OF_THE_STORM_ZONENAME, Node.Graveyard, self)
    self.nodes[EYE_OF_THE_STORM_ZONENAME].status = playerFaction
    self.window.centralItem.model = EFrame.ListModel(self)
    if StrategosProfile.arathiWindowHeight then
        self.window.height = EFrame.normalizeBind(StrategosProfile.arathiWindowHeight)
    end
    if StrategosProfile.arathiWindowWidth then
        self.window.width = EFrame.normalizeBind(StrategosProfile.arathiWindowWidth)
    end
    if StrategosProfile.arathiWindowX then
        self.window.marginLeft = EFrame.normalizeBind(StrategosProfile.arathiWindowX)
    end
    if StrategosProfile.arathiWindowY then
        self.window.marginTop = EFrame.normalizeBind(StrategosProfile.arathiWindowY)
    end
    self.window:connect("marginTopChanged", function(v) StrategosProfile.arathiWindowY = EFrame.normalized(v) end)
    self.window:connect("marginLeftChanged", function(v) StrategosProfile.arathiWindowX = EFrame.normalized(v) end)
    self.window:connect("heightChanged", function(v) if self.window.implicitHeight ~= v then StrategosProfile.arathiWindowHeight = EFrame.normalized(v) end end)
    self.window:connect("widthChanged", function(v) if self.window.implicitWidth ~= v then StrategosProfile.arathiWindowWidth = EFrame.normalized(v) end end)
    self.ttw = EFrame.Rectangle()
    local anchorFrame = UIWidgetTopCenterContainerFrame
    self.ttw.anchorTop = {frame = anchorFrame, point = "BOTTOM"}
    self.ttw.width = 250
    self.ttw.height = 20
    self.ttw.bar = EFrame.ProgressBar(self.ttw)
    self.ttw.bar.texture = "Interface\\TargetingFrame\\UI-StatusBar"
    self.ttw.bar.anchorFill = self.ttw
    self.ttw.color = EFrame.bind(function() return AB_BAR_BG_COLORS[self.winningTeam] end)
    self.ttw.bar.color = EFrame.bind(function() return AB_BAR_COLORS[self.winningTeam] end)
    self.ttw.text = EFrame.Label(self.ttw)
    self.ttw.text.text = EFrame.bind(function ()
        if self.winningTeam == Alliance then
            return self.allianceTimeToWin > 0 and format(tr("Alliance will win in: %02d:%02d"), math.floor(self.allianceTimeToWin/60), self.allianceTimeToWin % 60) or tr("Alliance won!")
        elseif self.winningTeam == Horde then
            return self.hordeTimeToWin > 0 and format(tr("Horde will win in: %02d:%02d"), math.floor(self.hordeTimeToWin/60), self.hordeTimeToWin % 60) or tr("Horde won!")
        else
            return tr("Stalling")
        end
    end)
    self.ttw.bar.from = 0
    self.ttw.bar.to = 10000
    EFrame.root:connect("update", self.ttw, "update")
    function self.ttw.update()
        local a, h = self.team[Alliance].winTime, self.team[Horde].winTime
        if not a and not h then
            self.winningTeam =  EyeOfTheStorm.Stalling
            self.ttw.bar.value = 0
            return
        end
        if a then
            a = self.team[Alliance].winTime - GetTime()
            self.allianceTimeToWin = a
        end
        if h then
            h = self.team[Horde].winTime - GetTime()
            self.hordeTimeToWin = h
        end
        if not a then
            self.winningTeam = EyeOfTheStorm.HordeAhead
            self.ttw.bar.value = 10000
        elseif not h then
            self.winningTeam = EyeOfTheStorm.AllianceAhead
            self.ttw.bar.value = 10000
        else
            self.winningTeam = self.allianceTimeToWin < self.hordeTimeToWin and EyeOfTheStorm.AllianceAhead or self.allianceTimeToWin > self.hordeTimeToWin and EyeOfTheStorm.HordeAhead or EyeOfTheStorm.Stalling
            if a < 0 or h < 0 then
                self.ttw.bar.value = 10000
            else
                self.ttw.bar.value = (1 - math.min(a, h)/math.max(h,a))*10000
            end
        end
    end
    
    self.ttw.text.anchorCenter = self.ttw
    self.b = SpiritHealerIndicator()
    self.b.anchorTop = self.ttw.bottom
    self:updateWorldStates()
    self.team = {[Alliance] = {}, [Horde] = {}}
    self.startTimer:clearAnchors()
    self.startTimer.marginTop = 14
    self.startTimer.anchorTop = self.b.bottom
    
    
    EventHandler:RegisterEvent("ARENA_OPPONENT_UPDATE")
    function EventHandler.ARENA_OPPONENT_UPDATE(n, s)
        local i = n == "arena2" and Alliance or n == "arena1" and Horde
        if i then
            local flag = self.flags[3 - i]
            if s ~= "cleared" then
                flag:pick(n)
                self.flags[i]:reset()
            else
                if flag.status == Flag.Carried then
                    flag:drop()
                end
            end
        end
    end
    for k,v in ipairs(carriersIds) do
        local flag = self.flags[k]
        if UnitExists(v) then
            flag:pick(v)
        end
    end
end

function EyeOfTheStorm:destroy()
    if WOW_PROJECT_ID ~= WOW_PROJECT_CLASSIC then
        EventHandler:UnregisterEvent("UNIT_NAME_UPDATE")
    end
    self.l1:destroy()
    self.l2:destroy()
    self.b:destroy()
    self.ttw:destroy()
    self.window:destroy()
    Battleground.destroy(self)
end

local evs = {
    [tr("The battle will begin in 1 minute")] = function (self)
        self.status = Battleground.Starting
        self:starting(60)
    end,
    [tr("The battle will begin in 30 seconds")] = function (self)
        self.status = Battleground.Starting
        self:starting(30)
    end,
    [tr("The battle has begun")] = function (self)
        self.status = nil
        self:started()
    end,
    [tr("has taken the flag")] = function (self, message, faction)
    end,
    [tr("dropped")] = function (self, message, faction)
        local flag = self.flags[faction]
        flag:drop()
        if faction ~= playerFaction then
            self.scan = false
        end
    end,
    [tr("returned")] = function (self, message, faction)
        local flag = self.flags[faction]
        flag:place()
        if faction ~= playerFaction then
            self.scan = false
        end
    end,
    [tr("captured")] = function (self)
        self.flags[Alliance]:reset()
        self.flags[Horde]:reset()
        self:restarting(10)
        self.status = self.Starting
        self.scan = false
    end,
    [tr("reset")] = function (self, message, faction)
        if strfind(message, tr("Horde")) then
            self.flags[Horde]:place()
            if faction == Alliance then
                self.scan = false
            end
        elseif strfind(message, tr("Alliance")) then
            self.flags[Alliance]:place()
            if faction == Horde then
                self.scan = false
            end
        else
            self.flags[Alliance]:place()
            self.flags[Horde]:place()
            self.status = nil
            self.scan = false
        end
    end
}

function EyeOfTheStorm:processChatEvent(message, faction)
    for s,f in pairs(evs) do
        local r = {strfind(message, s)}
        if #r > 0 then
            f(self, message, faction, select(3,unpack(r)))
            return
        end
    end
    Battleground.processChatEvent(self, message, faction)
end

local scoreKitFaction = {[5248] = Alliance, [5249] = Horde}

local ppb = {1, 2, 5, 10}

function EyeOfTheStorm:updateWorldStates(payload)
    if not self.b.ok then
        self.b:restart(self.nodes[EYE_OF_THE_STORM_ZONENAME])
        self.b.ok = true
    end
    local a = C_Map.GetBestMapForUnit("player")
    if a then
        for _,v in pairs(C_AreaPoiInfo.GetAreaPOIForMap(a)) do
            local i = C_AreaPoiInfo.GetAreaPOIInfo(a, v)
            if self.nodes[i.name] then
                self.nodes[i.name].status = POIInfo[i.textureIndex].status
            end
        end
    end
    
    if payload then
        local now = GetTime()
        local info = C_UIWidgetManager.GetIconAndTextWidgetVisualizationInfo(payload.widgetID)
        local msg = info.text
        local i = scoreKitFaction[info.textureKitID]
        if not msg then
            return
        end
        local _,_, n, s = strfind(msg, "(%d).-%D(%d+)/")
        if not n then return end
        n = tonumber(n)
        s = tonumber(s)
        if self.team[i].lastN ~= n or self.team[i].lastScore ~= s then
            if self.team[i].lastScore ~= s then
                self.team[i].lastScore = s
                self.team[i].lastTime = now
            end
            if n == 0 then
                self.team[i].winTime = nil
            else
                local tt = 2
                local pp = ppb[n]
                local w = math.ceil((2000 - s) / pp) * tt
                self.team[i].winTime = self.team[i].lastTime + w
            end
        end
    end
end

local grave = setmetatable({
}, { __index = function(_,k) return k end })

local lastSpiritHealerTime = 0
function EyeOfTheStorm:update()
    local node = self.nodes[grave[GetSubZoneText()]] or self.nodes[grave[EYE_OF_THE_STORM_ZONENAME]]
    local time = GetAreaSpiritHealerTime()
    for k,v in ipairs(carriersIds) do
        if UnitExists(v) then
            self.carriers[3 - k].health = UnitHealth(v)/UnitHealthMax(v) * 100
        else
            self.carriers[3 - k].health = nil
        end
    end
    if lastSpiritHealerTime and lastSpiritHealerTime ~= time and time > 1 and time < 30 then
        if not node then return end
        lastSpiritHealerTime = time
        local off = node:spiritHealerTime() - time -1
        if math.abs(off) > .25 then
            node.ressoff = node.ressoff + off
        end
    elseif time ~= 0 then
        lastSpiritHealerTime = time
    else
        lastSpiritHealerTime = nil
    end
    if node and self.b.node ~= node and node.status == playerFaction and (GetAreaSpiritHealerTime() > 0 or node.name ~= EYE_OF_THE_STORM_ZONENAME) then
        self.b:restart(node)
    end
end

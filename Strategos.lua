local addonName, addonTable = ...

_G[addonName] = addonTable

setmetatable(addonTable, {__index = getfenv() })
setfenv(1, addonTable) 

Strategos = EFrame.Object()
Strategos:attach("currentBattleground")

EventHandler = CreateFrame("Frame")
local Strategos_EventList = {
    "ZONE_CHANGED_NEW_AREA",
    "ZONE_CHANGED",
    "CHAT_MSG_BG_SYSTEM_NEUTRAL",
    "CHAT_MSG_BG_SYSTEM_ALLIANCE",
    "CHAT_MSG_BG_SYSTEM_HORDE",
    "CHAT_MSG_MONSTER_YELL",
    "PLAYER_ENTERING_WORLD",
    "UPDATE_UI_WIDGET",
    "UPDATE_BATTLEFIELD_SCORE",
    "GOSSIP_SHOW",
    "AREA_POIS_UPDATED"
}

EventHandler:RegisterEvent("ADDON_LOADED")

function overridePin(pin, useClassColors, size)
    if pin.strategosReplaced then return end
    pin.strategosReplaced = true
    if size then
        local old = GroupMembersPinMixin.SynchronizePinSizes
        function pin:SynchronizePinSizes()
            old(self)
            local scale = self:GetMap():GetCanvasScale();
            pin:SetPinSize("party", size/scale)
            pin:SetPinSize("raid", size/scale)
        end
        pin:SynchronizePinSizes()
    end
    pinTextureReplace(pin)
    if useClassColors() then
        pin:SetUseClassColor("party", true)
        pin:SetUseClassColor("raid", true)
    end
end

function overrideMapPins(frame, useClassColors, size)
    for pin in frame:EnumeratePinsByTemplate("GroupMembersPinTemplate") do
        overridePin(pin, useClassColors, size)
    end
    for pin in frame:EnumeratePinsByTemplate("SpectatorPinTemplate") do
        if not pin.strategosReplaced then
            pin.strategosReplaced = true
            local old = pin.UpdateAppearanceData
            function pin:UpdateAppearanceData()
                old(self)
                self:SetPinTexture("party","")
                self:SetPinTexture("raid","")
            end
            pin:UpdateAppearanceData()
        end
    end
end

function overrideMapPinsHook(frame, useClassColors, size)
    hooksecurefunc(frame, "AcquirePin", function(self) overrideMapPins(self, useClassColors) end)
    overrideMapPins(frame, useClassColors, size)
end

local function hookBattlefieldMap()
    local done
    BattlefieldMapFrame:HookScript("OnShow", function(self) if not done then C_Timer.After(1, function() overrideMapPinsHook(self, function() return StrategosProfile.bgMapUseClassColors end, 14) end) done = true end end)
end

function EventHandler.ADDON_LOADED(name)
    if name == addonName then
        if not _G.StrategosSettings then
            _G.StrategosSettings = {}
        end
        setmetatable(StrategosSettings, {__index = DEFAULT_SETTINGS})
        if not _G.StrategosProfile then
            _G.StrategosProfile = {}
        end
        setmetatable(StrategosProfile, {__index = DEFAULT_PROFILE})
        
        if WOW_PROJECT_ID == WOW_PROJECT_CLASSIC then
            overrideMapPinsHook(WorldMapFrame, function() return StrategosProfile.worldMapUseClassColors end)
        end
        if IsAddOnLoaded("Blizzard_BattlefieldMap") then
            if WOW_PROJECT_ID == WOW_PROJECT_CLASSIC then
                hookBattlefieldMap()
            end
        end
    elseif name == "Blizzard_BattlefieldMap" then
        if IsAddOnLoaded(addonName) then
            if WOW_PROJECT_ID == WOW_PROJECT_CLASSIC then
                hookBattlefieldMap()
            end
        end
    end
end

EventHandler:SetScript("OnEvent",function(self, event, ...)
    local handle = EventHandler[event]
    if handle then
        handle(...)
    elseif StrategosSettings.debug then
        print("unhandled event \""..event.."\"")
    end
end)

EventHandler:SetScript("OnUpdate", function()
    if Strategos.currentBattleground then
        Strategos.currentBattleground:update()
    end
end)

function register()
    for _, e in pairs(Strategos_EventList) do
        EventHandler:RegisterEvent(e)
    end
end
register()
function unregister()
    for _, e in pairs(Strategos_EventList) do
        EventHandler:UnregisterEvent(e)
    end
end

idLookups = {}

local loadTime = GetTime()
local function loadBG()
    local id = select(8,GetInstanceInfo())
    if Strategos.currentBattleground and Strategos.currentBattleground.id ~= id then
        Strategos.currentBattleground:finished()
        Strategos.currentBattleground:destroy()
        Strategos.currentBattleground = nil
    end
    if not Strategos.currentBattleground and idLookups[id] then
        Strategos.currentBattleground = idLookups[id]()
    end
end

function EventHandler.ZONE_CHANGED_NEW_AREA()
    if loadTime ~= GetTime() then
        C_Timer.After(0, function() EFrame:invokeNoCombat(loadBG) end)
    else
        loadBG()
    end
end

EventHandler.PLAYER_ENTERING_WORLD = EventHandler.ZONE_CHANGED_NEW_AREA
EventHandler.ZONE_CHANGED = EventHandler.ZONE_CHANGED_NEW_AREA

EventHandler.CHAT_MSG_BG_SYSTEM_NEUTRAL = function(msg) handleMessage(msg, 0) end
EventHandler.CHAT_MSG_BG_SYSTEM_ALLIANCE = function(msg)  handleMessage(msg, 1) end
EventHandler.CHAT_MSG_BG_SYSTEM_HORDE = function(msg)  handleMessage(msg, 2) end
EventHandler.CHAT_MSG_MONSTER_YELL = function(msg)  handleMessage(msg, -1) end

function handleMessage(message, faction)
    if faction <= 0 then
        if strfind(message, tr("[aA]lliance")) then
            faction = 1
        elseif strfind(message, tr("[Hh]orde")) then
            faction = 2
        end
    end
    if Strategos.currentBattleground then
        Strategos.currentBattleground:processChatEvent(message,  faction)
    end
end

function EventHandler.GOSSIP_SHOW()
    local info = C_GossipInfo.GetOptions()
	for i, v in pairs(info) do
		if v.icon == 132051 then
			C_GossipInfo.SelectOption(v.gossipOptionID)
		end
	end
end

function EventHandler.UPDATE_UI_WIDGET(payload)
    if Strategos.currentBattleground then
        Strategos.currentBattleground:updateWorldStates(payload)
    end
end

function EventHandler.AREA_POIS_UPDATED()
    if Strategos.currentBattleground then
        Strategos.currentBattleground:updateWorldStates()
    end
end

function EventHandler.UPDATE_BATTLEFIELD_SCORE()
    if Strategos.currentBattleground then
        Strategos.currentBattleground:updateScore()
    end
end

local partyTexture = "Interface/AddOns/Strategos/Textures/DotNew"
function pinTextureReplace(pin)
    
    local old = pin.GetUnitColor
    pin:SetUseClassColor("party", false)
    pin:SetUseClassColor("raid", false)
    function pin:GetUnitColor(a, unit, appearanceData)
        v, r, b, g = old(self, a, unit, appearanceData)
        if not v then return false end
        if unit ~= "player" and (not appearanceData.useClassColor ~= (IsShiftKeyDown() or IsAltKeyDown() or IsControlKeyDown())) then
            return true, GetClassColor(select(2,UnitClass(unit)))
        end
        if unit == "player" then return v, r, b, g end
        if UnitIsGhost(unit) then
            r, g, b = 0.3, 0.3, 1
        elseif UnitIsDead(unit) then
            r, g, b = 0.7, 0.7, 1
        else
            local p = UnitHealth(unit)/UnitHealthMax(unit)
            r, g, b = (1 - p)*2, p*2, 0
        end
        return true, r, g, b
    end
    local old = pin.UpdateAppearanceData
    function pin:UpdateAppearanceData()
        old(self)
        self:SetPinTexture("party",partyTexture)
        self:SetPinTexture("raid",partyTexture)
    end
    pin:UpdateAppearanceData()
end

_G.SLASH_STRATEGOS1 = "/strategos"
_G.SLASH_STRATEGOS2 = "/stg"
SlashCmdList["STRATEGOS"] = function (msg)
    if msg == "worldMap toggleClassColors" then
        for pin in WorldMapFrame:EnumeratePinsByTemplate("GroupMembersPinTemplate") do
            StrategosProfile.worldMapUseClassColors = not StrategosProfile.worldMapUseClassColors
            pin:SetUseClassColor("party", StrategosProfile.worldMapUseClassColors)
            pin:SetUseClassColor("raid", StrategosProfile.worldMapUseClassColors)
        end
    elseif msg == "bgMap toggleClassColors" then
        StrategosProfile.bgMapUseClassColors = not StrategosProfile.bgMapUseClassColors
        for pin in BattlefieldMapFrame:EnumeratePinsByTemplate("GroupMembersPinTemplate") do
            pin:SetUseClassColor("party", StrategosProfile.bgMapUseClassColors)
            pin:SetUseClassColor("raid", StrategosProfile.bgMapUseClassColors)
        end
    end
end

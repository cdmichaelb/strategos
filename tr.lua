function getfenv() end
function setfenv() end
function setmetatable() end
Localizations = {}
dofile(arg[1])

function decorate(x)
    if type(x) == "string" then
        return string.format("\"%s\"",x:gsub('"', '\\"'))
    end
    return x
end

for k, v in pairs(Localizations[arg[2]]or{}) do
    print("|"..decorate(k))
    for k2, v2 in pairs(v) do
        print("  |"..decorate(k2))
        for k3, v3 in pairs(v2) do
            print("    |"..decorate(k3))
            print("      |"..decorate(v3))
        end
    end
end
